#!/usr/bin/python

Hunger = 6
Interest = 6
Love = 6
Mood = "Happy"
oldMood = "Happy"

def addCap(v1,value):
    if v1 + value > 10:
        return 10
    else:
        return v1 + value

def interalStateChange(state, hunger, happy, love):
    if state == "Happy":
        hunger = hunger - 1
        happy = happy - 1
    elif state == "Hungry":
        hunger = hunger - 0.5
        happy = happy - 1
        love = love - 2
    elif state == "Bored":
        hunger = hunger - 1
        happy = happy - 2
        love = love - 1
    elif state == "Excited":
        hunger = hunger - 2
        happy = happy - 1
        love = addCap(love, 1)
    
    return hunger, happy, love
        

def commandEffect(command, state, hunger, happy, love):
    if state == "Happy":
        if command == "F":
            hunger = addCap(hunger, 2)
        elif command == "P":
            happy = addCap(happy, 2)
            love = addCap(love, 2)
        elif command == "C":
            happy = addCap(happy, 3)
            
            
    elif state == "Hungry":
        if command == "F":
            hunger = addCap(hunger, 2)
            love = addCap(love, 2)
            
        # p no effect
            
        elif command == "C":
            happy = addCap(happy, 3)
            
            
    elif state == "Bored":
        if command == "F":
            hunger = addCap(hunger, 1.5)
        elif command == "P":
            happy = addCap(happy, 2)
            love = addCap(love, 1)
        elif command == "C":
            happy = addCap(happy, 3)
            
            
    elif state == "Excited":
        if command == "F":
            hunger = addCap(hunger, 2)
        elif command == "P":
            happy = addCap(happy, 2)
            love = addCap(love, 2)
        elif command == "C":
            happy = addCap(happy, 5)
            love = addCap(love, 2)
            
    return hunger, happy, love




def stateTransition(state, hunger, happy, love):
    if state == "Happy":
        if hunger <= 4:
            return "Hungry"
        elif happy <= 3:
            return "Bored"
        elif happy >= 7:
            return "Excited"
    
    elif state == "Hunger":
        if hunger <= 0:
            return "Dead"
        elif hunger >= 7:
            return "Happy"
        elif happy >= 7:
            return "Excited"
    
    elif state == "Bored":
        if happy >= 6:
            return "Excited"
        if happy >= 4:
            return "Happy"
        if hunger < 0 or happy < 0 or love < 0:
            return "Dead"
        
    elif state == "Excited":
        if happy <= 5:
            return "Happy"
        elif hunger <= 0:
            return "Dead"
            
    return state        
            
try:
    print("Hunger, Interest, Love, OldState, State")
    while True:
        oldMood = Mood
        Hunger, Interest, Love = interalStateChange(Mood, Hunger, Interest, Love)
        inCommand = input()
        print("command = " + inCommand)
        Hunger, Interest, Love = commandEffect(inCommand, Mood, Hunger, Interest, Love)
        Mood = stateTransition(Mood, Hunger, Interest, Love)
        arr = [str(Hunger), str(Interest), str(Love), oldMood, Mood]
        output = ",".join(arr)
        print(output)
        if Mood == "Dead":
            print("pet is dead! see you")
            break
except EOFError:
    print("end")