#!/usr/bin/python

import sys
import subprocess
import os 

def runShell(command):
    result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
    return result.stdout.decode()
    
def coverCsv(text):
    rows = text.split("\n")
    for i in range(len(rows)):
        rows[i] = rows[i].split(",")
    rows = rows[1:len(rows) - 1]
    return rows
    
state = ""

try:
    while True:
        inputLine = input()
    
        data = inputLine.split(",")
    
        fileName = data[0]
    
        path = data[1]
    
        checkSum = data[2]
    
        # find all file 
        
        command = "find ./ -type f"
        
        allFile = runShell(command).split("\n")
        
        targetPath = "./" + path + fileName
        
        command = "sha1sum " + targetPath + " | cut -d ' ' -f 1 "
        
        realSum = runShell(command).split()[0]
        
        correctNum = 0
        correctLocation = 0
        for f in allFile:
            if f == "":
                continue
            
            fPath = f.split("/")
            fPath = fPath[0:len(fPath) - 1]
            fPath = "/".join(fPath)
            
            fName = f.split("/")
            # print(fName)
            fName = fName[len(fName) - 1]
            
            # print("realPath = " + f)
            # print("targetPath = " + targetPath)
            # print("realSum = " + realSum)
            # print("checkSum = " + checkSum)
            # print("fName " + fName)
            # print("\n")
            
            if f == targetPath and realSum == checkSum:
                correctNum = correctNum + 1
            elif fName == fileName and targetPath != f and realSum == checkSum:
                correctNum = correctNum + 1
            elif f == targetPath and realSum != checkSum:
                correctLocation = correctLocation + 1
                
        if correctNum == 1:
            state = "correct"
        elif correctNum > 1:
            state = "correct_sum"
        elif correctLocation > 0:
            state = "correct _location"
        else:
            state = "error"
        
        print(",".join([fileName, path, state]))
except:
    print("end")