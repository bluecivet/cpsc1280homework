#!/bin/bash

find -name "dir*" -type d -prune | xargs rm -rf 

mkdir dir1 dir2

mkdir -p dir1/{a..b} && mkdir dir1/g && touch dir1/a/a{1..3}.txt && touch dir1/b/b{1..3}.txt && touch dir1/g/c{1..3}.txt

mkdir -p dir2/{a..c} && touch dir2/a/a{1..3}.txt && touch dir2/b/f{1..3}.txt && touch dir2/c/c{1..3}.txt

