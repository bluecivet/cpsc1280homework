#!/bin/bash

# find all file
#find $1 -type f | rev | cut -d "/" -f 1 | rev | sort >> dir1Temp
find $1 -type f | cut -d "/" -f 2- | sort >> dir1Temp
#find $2 -type f | rev | cut -d "/" -f 1 | rev | sort >> dir2Temp
find $2 -type f | cut -d "/" -f 2- | sort >> dir2Temp

comm dir1Temp dir2Temp -2 -3 | tr -s "\n" > $3
comm dir1Temp dir2Temp -3 -1 | tr -s "\n" > $4
comm dir1Temp dir2Temp -1 -2 | tr -s "\n" > $5

rm -f *Temp



