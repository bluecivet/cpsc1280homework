find -name "testDir3" -type d -prune -exec rm -rf {} \;

mkdir testDir3

cd testDir3

mkdir -p d1/d11/d111 d1/d12/d121
mkdir -p d2/d21/d211 d2/d22/d221 d2/d23

touch d1/f{1..3}
touch d1/d11/f{4..5}
touch d1/d11/d111/f{6..9}
touch d1/d12/f{10..14}
touch d1/d12/d121/f{15..20}


ln d1/f1 d2/d22/f1.link     # link 3
ln d1/f1 d2/d22/f1.link2
ln d1/f1 d2/d22/f1.link3

ln d1/d11/f4 d2/f4.link
ln d1/d11/f5 d2/f5.link

ln d1/d11/d111/f6 d2/d22/d221/f6.link   # link4
ln d1/d11/d111/f6 d2/d22/d221/f6.link2
ln d1/d11/d111/f6 d2/d22/d221/f6.link3
ln d1/d11/d111/f6 d2/d22/d221/f6.link4

ln d1/d11/d111/f7 d2/d22/d221/f7.link
ln d1/d11/d111/f8 d2/d22/d221/f8.link

ln d1/d11/d111/f9 d2/d22/d221/f9.link   # link 2
ln d1/d11/d111/f9 d2/d22/d221/f9.link2


ln d1/d12/f10 d2/d21/f10.link
ln d1/d12/f11 d2/d21/f11.link
ln d1/d12/f12 d2/d21/f12.link
ln d1/d12/f12 d2/d21/f12.link2
ln d1/d12/f13 d2/d21/f13.link
ln d1/d12/f14 d2/d21/f14.link


ln d1/d12/d121/f15  d2/d21/d211/f15.link
ln d1/d12/d121/f16  d2/d21/d211/f16.link
ln d1/d12/d121/f17  d2/d21/d211/f17.link

ln d1/d12/d121/f18  d2/d23/f18.link
ln d1/d12/d121/f18  d2/d23/f18.link2    # link2

