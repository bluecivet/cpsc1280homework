#!/bin/bash

tail -n+2 $1 | sort -n -k 2 > $1.sorted
cat $1.sorted | rev | cut -d "," -f 2 | rev >> genderTemp
cat $1.sorted | cut -d "," -f 1 >> yearTemp
cat $1.sorted | cut -d "," -f 4 >> neiNTemp
cat $1.sorted | cut -d \" -f 10 >> neiTemp
cat $1.sorted | rev | cut -d "," -f 1 | rev >> numberTemp

echo "the number of total birth is: " $(($(cat $1 | wc -l )-1))
paste genderTemp yearTemp neiNTemp neiTemp numberTemp 

rm $1.sorted *Temp