#!/bin/bash

find ./ -name "output.csv" -exec rm {} \;

category=$(tail -n +2 $1 | cut -d "," -f 2 | sort -u)

# echo $category;

category=($category)

# echo ${#category[*]}



while read line 
do
    echo $line | grep -q "^.*,.*$"
   
    if [ $? -eq 0 ] 
    then
        left=$(echo $line | cut -d "," -f 1)
        right=$(echo $line | cut -d "," -f 2)
        outputLine=$left
    
      for c in ${category[*]}
        do
            if [ $c == $right ]
            then 
                outputLine=$outputLine',1'
            else
                outputLine=$outputLine',0'
            fi
        done
        
        echo $outputLine >> output.csv
    else
        echo "enter not find"
        echo $line >> ouput.csv
    fi
    
    
done < $1

outputFirstLine=$(head -n1 $1 | cut -d "," -f 1)

for c in ${category[*]}
do 
    outputFirstLine=$outputFirstLine','$c
done

# echo $outputFirstLine
sed -e "1c $outputFirstLine" output.csv > temp

cat temp > output.csv

rm temp

unset $category
