mkdir script1;

# 100
cd script1;
touch rabbit;
mkdir Crab Noah Shrimp;

#200
cd Noah;
touch cheese brimble pepper rice beans;
chmod u=rwx cheese rice beans;
chmod u=wx brimble;
chmod u=rx pepper;
cd ..;


#270 
cd Crab;
touch Coven Diet Covention;
mkdir Conclave Senate;   


#300
cd Senate;
touch TrainerTips EngineeringExplained;
ln ../Coven CathyCat;
chmod u=r *;
chmod u=rwx CathyCat;
cd ..;

#350 
cd Conclave;
ln ../../rabbit Chocolate;
ln ../Covention Strawberry;
touch Caramel;
chmod u=rwx Chocolate;
chmod u=rw Strawberry;
chmod u=r Caramel;
cd ..;

chmod u=rwx Coven Conclave;
chmod u=rw Diet Covention;
chmod u=rx Senate;

cd ..

#700
cd Shrimp;
touch steel copper;
mkdir iron;
chmod u=rw steel;

#240
cd iron;
touch tiger rabbit wolf rat;
chmod u=r tiger rabbit;
chmod u=rw wolf rat;
cd ..;

chmod u=rx copper iron;

cd ..;

chmod u+rwx Noah;
chmod u+rwx rabbit;
chmod u=r Crab;
chmod u=rx Shrimp; 