1.	What are the 3 permissions that you can modify with chmod, and what does each one do relative to a file?
        
        read, write, and execute permission. 
        read for reading the content of a file.
        wirte for adding delete and changing file content
        execute for runing a file like scripe or program file


2.	What does the execute attribute control for a directory file?
        
        read for loading the directory content
        write for adding deleteing and changing directory content
        execute for accessing the permission for directory content


3.	A root user can arbitrarily change the permissions on any file arbitrarily at any time. Why might a root user, set the root permissions of files to read only, even through she can change them later.

        because there may be an accident or program that can change the file and the root user want to make sure that will not happen because root user donot want to change the file.



4.	What is the difference between a hard link and a symbolic link, with respect the rm command. (rm is used to delete files)

        hard link create a filename which point to same inode 
        soft link create a new file different inode and its content the part of linked file
        
        user use rm to a hard link just decrease the link count number to orginal file.
        but rm on soft link can remove orginal file


5.	Given the list of files listed below, write a pattern( otherwise known as a glob) to match the files in the first list, but not the second list
Match
•	dog
•	cat
•	pig
•	rat
Do not match
•	cake
•	sugar
•	cookie
•	chocolate
•	pastry

because it is only 3 letter
answer: ???


6.	Given the list of files listed below, write a pattern( otherwise known as a glob) to match the files in the first list, but not the second list
Match
•	flasher
•	flour
•	flyer
•	flalafel spinner
•	four dinner
Do not match
•	foundery
•	biter
•	bitter
•	flippant
•	market
•	umbrella
•	that
•	finger

because it start with f and end with r
answier: {fl*r,fo*r}


7.	Is it possible for a user to know how many symbolic link there is to a file if there is only one hard link to the file? 
•	If yes, what must user do?
•	If no, why not?
        
        yes, first find all the link in the system and list them out
        grep the file name then count how many line in result.
        
        find -type l -exec ls -l {} \; | grep the filename | wc -l 
