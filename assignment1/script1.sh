 #!/bin/bash

mkdir script1;
cd script1;

mkdir -p Pumkpin/kobocha Pumkpin/WinterMelon;
chmod 555 Pumkpin;

mkdir -p Diakon/Pickle Diakon/Oden;
chmod 700 Diakon;

mkdir -p Carrot/Orange Carrot/Oden;
chmod 000 Carrot;

mkdir -p Parsenip/Chips Parsenip/Soup;
chmod 555 Parsenip;

echo "finish creating";

echo "start copying file";

cp ../../Datasets/pg58.txt ./Diakon/Pickle;
echo 'echo Pickle';
ls -l Diakon/Pickle;

echo "finish copy pg58.txt";

chmod 700 Carrot;
cp ../../Datasets/pg972.txt ./Carrot;
cp ../../Datasets/pg972.txt ./Carrot/Oden;

echo 'echo Carrot';
ls -l Carrot;
echo 'echo Carrot/Oden';
ls -l Carrot/Oden;

chmod 000 Carrot;

echo "finish copy pg972.txt";

chmod 700 Pumkpin;
cp ../../Datasets/heart.csv ./Pumkpin;
echo 'echo Pumkpin';
ls -l Pumkpin;
chmod 555 Pumkpin;


chmod 700 Parsenip;
cp ../../Datasets/heart.csv Parsenip/Chips;
echo 'echo Parsenip/Chips';
ls -l Parsenip/Chips;
chmod 555 Parsenip;

echo "finish copy heart.csv";