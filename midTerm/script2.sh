#/bin/bash

echo "" > tamountTemp.temp



peopleList=($(tail -n +2 $1 | cut -d "," -f 1 | sort -u | xargs))

amount=()
test=()

#init amount
count=0
for saler in ${peopleList[@]}
do
    amount[$count]=0
    echo 0 >> amountTemp.temp
    count=$(($count + 1))
done


tail -n +2 $1 | 
while read line
do 
    people=$(echo $line | cut -d "," -f 1)
    money=$(echo $line | cut -d "," -f 6)
    saleId=$(echo $line | cut -d "," -f 2)
    saleIdFormat=$(echo $saleId | bc)
    cat $2 | grep -q -E "^$saleIdFormat,$money"
    if [ $? -eq 0 ]
    then
        continue;
    fi
    
    for (( i = 0; i < ${#peopleList[@]}; i++ )); do
        if [ ${peopleList[$i]} == $people ]
        then
            amount[$i]=$(echo "${amount[$i]} + $money" | bc)
            break;
        fi
    done
    
    echo "" > amountTemp.temp
    for m in ${amount[@]}
    do
        echo $m >> amountTemp.temp
    done
done


echo "SalesPerson ID,Total"
i=0;
amount=($(cat amountTemp.temp | xargs))
sum=0
for saler in ${peopleList[@]}
do
    echo "$saler,${amount[$i]}"
    sum=$(echo "$sum + ${amount[$i]}" | bc)
    i=$(($i + 1))
done
echo "Total,$sum"

rm tamountTemp.temp

