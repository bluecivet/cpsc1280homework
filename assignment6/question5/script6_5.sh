#!/bin/bash

path=$(echo $1 | rev | cut -d "/" -f 2- | rev)

touch sedScript
chmod 777 sedScript

file=$(cat $1 | grep "#include" | sed -e '/#include/s///' -e 's/<//' -e 's/>//' )
echo $file | xargs -n1 | xargs -i find $path/ -name {} | xargs -n1 ./scriptHelper.sh


sed -f sedScript $1 -e '/#include/d'

rm sedScript
