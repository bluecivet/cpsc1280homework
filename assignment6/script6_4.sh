#!/bin/bash

findText=$(cat immigrants_emigrants_by_destination2.csv | cut -d "," -f 1 | grep -n '^\"Ciutat Vella\"')

# echo $findText | xargs -n 1;

count=$(echo $findText | xargs -n 1 | wc -l)

lastLine=$(echo $findText | xargs -n 1 | cut -d ":" -f 1 | tail -n 1)

# echo $lastLine
# echo $count

sed -e "$lastLine a $count" immigrants_emigrants_by_destination2.csv > temp

cat temp > immigrants_emigrants_by_destination2.csv

rm -f temp