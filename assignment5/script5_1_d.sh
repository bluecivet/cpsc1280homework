#!/bin/bash

text=~/environment/cpsc1280homework/Datasets/BC_Liquor_Store_Product_Price_List.csv

# grep --color -E ',([2-9][0-9]|1[0-9]{2})\.?[0-9]*,[0-9A-Z]$' $text
grep --color -E ',([2-9][0-9]|1[0-9]{2})(|\.[0-9]+),[0-9A-Z]$' $text