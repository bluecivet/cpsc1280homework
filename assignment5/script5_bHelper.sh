#!/bin/bash

# find the text within the line ..,...,,"ttt (the ttt)
starttext=$(head -n$1 $2 | tail -n 1 | cut -d "," -f 6)

# get the start index ignore that next and start from next one
index=$1
startindex=$(($index+1))


# find the end index (this is other sond title)
endindex=$(tail -n +$startindex $2 | grep -En "^[0-9]{1,},[a-zA-Z_-]{1,},[0-9]{1,},[a-zA-Z_-]{1,},[a-zA-Z_-]{1,}," | head -n 1 | cut -d ":" -f 1)
# find the real endindex for the sone 
endindex=$(($endindex-1))

# start output 
echo $starttext
 tail -n +$startindex $2 | head -n $endindex
 
 echo " "
 echo " "