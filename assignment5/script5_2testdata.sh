#!/bin/bash

find ./ -name "testData1" | xargs rm -rf

mkdir testData1

cd testData1

mkdir d1 d2 d3

mkdir -p d1/d1{1..3} d2/d2{1..3} d3/d3{1..3}

touch d1/d11/aafound{1..3}bb

touch d1/d12/ccNotfound{1..3}bb

touch d1/d13/ccNotfound{1..3}dd


touch d2/d21/bbfound{1..3}aa

touch d2/d22/aaNotfound{1..3}dd

touch d2/d23/bbfound{1..3}aa


touch d3/d31/abNotfound{1..3}

touch d3/d32/Notfound{1..3}bb

touch d3/d33/Notfound{1..3}
