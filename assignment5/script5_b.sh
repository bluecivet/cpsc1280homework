#!/bin/bash

file="/home/ec2-user/environment/cpsc1280homework/Datasets/lyrics.csv"


# get all the song with the name $1
#find the line number for the sone in the file 
# pass the line number and file name to helper
grep --color -En "^[0-9]{1,},$1,[0-9]{1,},[a-zA-Z_-]{1,},[a-zA-Z_-]{1,}," $file | cut -d ":" -f 1 | xargs -i -n1 ./script5_bHelper.sh {} $file
