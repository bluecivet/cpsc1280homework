#!/bin/bash

date=$(grep -E "$2" $1 | cut -d "," -f 6-7)
echo $date
grep --color -E "$date" $1