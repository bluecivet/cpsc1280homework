#!/bin/bash

str1=$2
str2=$3

find $1 -type f | grep --color -E "($str1+.*$str2+)|($str2+.*$str1+)" | rev | cut -d "/" -f 2- | rev | uniq