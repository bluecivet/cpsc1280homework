#!/usr/bin/python

import re

def readFile():
	text = ""
	eof = False
	while(not eof):
		try:
			text = text + input("") + "\n"
		except EOFError:
			eof = True
	return text



def appendData(text, i, output):
# 	print("in append data")
# 	print("text i + 1 = " + text[i])
# 	print("text i + 2 = " + text[i + 1])
# 	print("text i + 3 = " + text[i + 2])
	num1 = float(text[i])
	num2 = float(text[i + 1])
	num3 = float(text[i + 2])
	j = i + 2
	output[len(output) - 1].append(str(num1))
	output[len(output) - 1].append(str(num2))
	output[len(output) - 1].append(str(num3))
	return j


def stateControl(currentState, keyword, text, i, output):
	# make sure keyword is in lower case
	keyword = keyword.lower()
	returnState = currentState
	j = i
# 	print("currentState = " + currentState + " keyWord = " + keyword + " i = " + str(i))

	if currentState == "ready":
		if keyword == "solid":
			returnState = "start"
	elif currentState == "start":
		if keyword == "facet":
			if text[i + 1] == "normal":
				returnState = "readNormal"
				j = i + 1
				# print("append new row")
				output.append([])
			else:
				returnState = "error";
	elif currentState == "readNormal":
		try:
			j = appendData(text, i, output)
			returnState = "finishReadNormal"
		except Exception:
			returnState = "error"

	elif currentState == "finishReadNormal":
		if keyword == "outer":
			if text[i + 1] == "loop":
				returnState = "startReadVertex"
				j = i + 1
			else:
				return "error"
	elif currentState == "startReadVertex" or currentState == "readVertex":
		if keyword == "vertex":
			returnState = "readVertex"
			try:
				j = appendData(text, i + 1, output)
			except Exception:
				returnState = "error"
		elif currentState == "readVertex":
			if keyword == "endloop":
				returnState = "finishReadVertex"
			else:
				returnState == "normal"

	elif currentState == "finishReadVertex":
		if keyword == "endfacet":
			returnState = "start"	# return to read normal state
		else:
			returnState = "error"
	elif currentState == "start":
		if keyword == "endsolid":
			returnState == "end"
			
	return returnState, j


# read the while file first

keyWordList = ["solid", "facet", "normal", "outer", "loop", "vertex", "endloop" "endfacet", "endsolid"]
text = readFile().split()

# print(text)

output = []
state = "ready"

i = 0
while i < len(text):
	word = text[i]
	state, i = stateControl(state, word, text, i, output)
	if state == "error":
		print("some error happen")
		break
	elif state == "end":
		break
	i = i + 1

# print(output)

print("normalX,normalY,normalZ,p1X,p1Y,p1Z,p2X,p2Y,p2Z,p3X,p3Y,p3Z")

outputText = ""
for row in output:
# 	print(row)
	outputText = outputText + ",".join(row) + "\n"

print(outputText)


