#!/usr/bin/python

import os 
import sys

try:
    
    while True:
        directpry = input()
        print(directpry)
        
        if not os.path.exists(directpry):
            sys.stderr.write("the path is not exist" + "\n")
            continue
        
        if not os.path.isdir(directpry):
            sys.stderr.write("this is not a directory" + "\n")
            continue
        
            
        fileList = os.listdir(directpry)
        
        size = 0
        count = 0
        for file in fileList:
            fpath = directpry + "/" + file
            
            if os.path.isdir(fpath):
               sys.stderr.write("this is a directory" + "\n")
               continue
            elif os.path.islink(fpath):
                sys.stderr.write("this is a link" + "\n")
                continue
            elif os.path.isfile(fpath):
                size = size + os.path.getsize(fpath)
                count = count + 1
        
        print("there are " + str(count) + " file and total size is " + str(size))
            
except EOFError:
    print("finish reading")
except:
    sys.stderr.write("error" + "\n")
