#!/usr/bin/python

import sys 

def text2list(text):
    data = text.split("\n")
    for i in range(len(data)):
        data[i] = data[i].split(",")
    return data

saleTablePath = sys.argv[1]
refundPath = sys.argv[2]

saleData = text2list(open(saleTablePath).read())
refundData = text2list(open(refundPath).read())

sales = []

people = {}


# init people

i = 1
while i < len(saleData):
    p = saleData[i][0]
    sales.append("")
    if p not in people and p != "":
        people[p] = 0
    
    i = i + 1


# process sale table
i = 1
while i < len(saleData):
    if saleData[i][0] == "":
        break
    
    saleId = int(saleData[i][1])
    person = saleData[i][0]
    amount = saleData[i][5]
    sales[saleId] = person
    people[person] = float(people[person]) + float(amount)
    
    i = i + 1
    
    
# process refund table

i = 1
while i < len(refundData):
    if refundData[i][0] == "":
        break
    saleId = int(refundData[i][0])
    amount = refundData[i][1]
    salePerson = sales[saleId]
    people[salePerson] = float(people[salePerson]) -float(amount)
    
    i = i + 1


peopleSorted = {}

sortedKey = sorted(people.keys())

for key in sortedKey:
    peopleSorted[key] = people[key]

people = peopleSorted


outputText = "SalesPerson ID,Total \n"
total = 0
for key in people:
    outputText = outputText + key + "," + str(people[key]) + "\n"
    total = total + float(people[key])

outputText = outputText + "Total," + str(total)

print(outputText)