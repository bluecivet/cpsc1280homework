#!/usr/bin/python

import sys
import subprocess

file = sys.argv[1]
outputFile = "output.csv"


def getOneHotLine(left, right, cArr):
    if right == "" or left == "":
        return""
    outText = left
    for c in cArr:
        if c == right:
            outText = outText + ",1"
        else:
            outText = outText + ",0"
    return outText



def runShell(command):
    result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
    result = result.stdout.decode().split("\n")
    return result[0:len(result) - 1]
    
command = "cat " + file + " | cut -d ',' -f 2 | sort -u"
categories = runShell(command)

output = open(outputFile, "wt")
f = open(file, "rt")
text = f.read()
lineText = text.split("\n")

firstLine = lineText[0]
leftName = firstLine.split(",")[0]

outputText = leftName + "," + ",".join(categories) + "\n"

# start from line 2
for i in range(1, len(lineText)):
    row = lineText[i]
    left = row.split(",")[0]
    if left == "":
        continue
    right = row.split(",")[1]
    left = row.split(",")[0]
    line = getOneHotLine(left ,right, categories)
    outputText = outputText + line + "\n"

output.write(outputText)
f.close()
output.close()