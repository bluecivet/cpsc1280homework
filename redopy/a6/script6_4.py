#!/usr/bin/python

import subprocess

f = "immigrants_emigrants_by_destination2.csv"

command = "cat " + f + "| grep -nE --color '^\"Ciutat Vella\",'"
result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
result = result.stdout.decode().split("\n")

count = len(result) - 1
start = result[0]
end = result[len(result) - 2]

endIndex = end.split(":")[0]

insertText = "count = " + str(count)
command = "cat " + f + "| sed -e '"+ endIndex +"a \"" + insertText + "\"'"
print(command)
subprocess.run(command, shell = True)