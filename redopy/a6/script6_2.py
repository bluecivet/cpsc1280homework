#!/usr/bin/python

import subprocess
import sys

file = sys.argv[1]

command = "cat " + file + " | grep -E '[a-zA-Z]+, adj.' | cut -d ',' -f 1"
subprocess.run(command, shell = True)