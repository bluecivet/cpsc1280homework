#!/usr/bin/python

def coverCsv(text):
    rows = text.split("\n")
    for i in range(len(rows)):
        rows[i] = rows[i].split(",")
    rows = rows[1:len(rows) - 1]
    return rows
    
    
file = "BlackFriday.csv"
f = open(file)
text = f.read()
print("finish reading")
data = coverCsv(text)
print("finish converting")


output = ""
for row in data:
    uid = row[0]
    pid = row[1]
    price = row[len(row) - 1]
    outRow = [uid, pid, price]
    # output = output + ",".join(outRow) + "\n"
    print(",".join(outRow) + "\n")

print(output)
