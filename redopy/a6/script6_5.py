#!/usr/bin/python

import sys
import subprocess 

def runShell(command):
    result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
    result = result.stdout.decode().split("\n")
    return result[0:len(result) - 1]


file = sys.argv[1]
filePath = file.split("/")
filePath = filePath[0:len(filePath) - 1]
filePath = "/".join(filePath)

command = "cat " + file + " | grep -nE '^#include<'"
includeFile = runShell(command)


fileNames = []
lineNums = []
for row in includeFile:
    name = row.split("<")[1]
    fileNames.append(filePath + "/" + name[0:len(name)-1])
    lineNums.append(row.split(":")[0])




sedCommand = ""
for i in range(len(fileNames)):
    # print(fileNames[i])
    # sedCommand = sedCommand + " -e '" + lineNums[i] + "d'"
    sedCommand = sedCommand + " -e '" + lineNums[i] + "r " + fileNames[i] + "' "
    

command = "cat " + file + " | sed " + sedCommand + " | sed /#include/d"

subprocess.run(command , shell = True)