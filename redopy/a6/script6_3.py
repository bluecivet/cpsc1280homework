#!/usr/bin/python

import sys
import subprocess

name = sys.argv[1]

file = "BirdsInHawaii.txt"
f = open(file)
text = f.read()

lineText = text.split("\n")


startline = 1
endline = 0
for i in range(len(lineText)):
    # print(lineText[i])
    if lineText[i].find(name) != -1:
        j = i 
        while j < len(lineText):
            if lineText[j].find("DESCRIPTION:") != -1:
                k = j 
                startline = j + 1
                while k < len(lineText):
                    if lineText[k].find("PARK DISTRIBUTION:") != -1:
                        endline = k
                        break
                    k = k + 1
                break
            
            j = j + 1
        break

command = "cat " + file + "| sed -n '" + str(startline) + "," + str(endline) + "p'"
print(command)
subprocess.run(command, shell = True)
f.close()