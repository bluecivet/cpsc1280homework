#!/usr/bin/python

import os
import subprocess
import sys
import shutil

sDir = sys.argv[1]
dDir = sys.argv[2]
outputFile = sys.argv[3]
outputFileTemp = outputFile + ".temp"

def runShell(command):
    result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
    return result.stdout.decode()

f = open(outputFileTemp, "wt")
date = runShell("date")
date = date.replace("\n", " ")
f.write(date + " source: " + sDir + " destination: " + dDir + "\n")

if os.path.exists(dDir):
    shutil.rmtree(dDir)

os.mkdir(dDir)

command = "find " + sDir + " -type d | cut -d '/' -f 2- | tail -n +2" 
dirs = runShell(command).split()

print(dirs)

for directory in dirs:
    path = dDir + "/" + directory
    print(path)
    os.mkdir(path)

command = "find " + sDir + " -type f | cut -d '/' -f 2- " 
targetLinkFile = runShell(command).split()

for file in targetLinkFile:
    sPath = sDir + "/" + file
    iNodeCommond = "ls -i " + sPath
    iNode = runShell(iNodeCommond)
    dPath = dDir + "/" + file + ".bak"
    command = "ln " + sPath + " " + dPath
    subprocess.run(command, shell = True)
    f.write(iNode + "  " + sPath + "  " + dPath + "\n")

f.close()

# add line number
command = "cat -n " + outputFileTemp + " > " + outputFile
subprocess.run(command, shell = True)

os.remove(outputFileTemp)

    
    
    