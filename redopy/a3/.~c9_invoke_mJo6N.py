#!/usr/bin/python

import os
import subprocess
import sys
import shutil

sDir = sys.argv[1]
dDir = sys.argv[2]
outputFile = sys.argv[3]

def runShell(command):
    subprocess.run(command, shell = True, stdout = subprocess.PIPE)
    return dirs.stdout.decode()

f = open(outputFile, "wt")
date = runShell("date")
f.write(date + " source: " + sDir + " destination: " + dDir + "\n")

if os.path.exists(dDir):
    shutil.rmtree(dDir)

os.mkdir(dDir)

command = "find " + sDir + " -type d | cut -d '/' -f 2- | tail -n +2" 
dirs = runShell(command)

for directory in dirs:
    path = dDir + "/" + directory
    os.mkdir(path)

command = "find " + sDir + " -type f | cut -d '/' -f 2- " 
targetLinkFile = runShell(command)

for file in targetLinkFile:
    sPath = sDir + "/" + file
    iNodeCommond = "ls -i " + sPath
    iNode = runShell(iNodeCommond)
    dPath = dDir + "/" + file + ".bak"
    command = "ln " + sPath + " " + dPath
    f.write(iNode + "  " + sPath + "  " + dPath)

f.close()

# add line number
cmooand = "cat -n " + 
    
    
    