 #!/bin/bash

find ./ -name "test" -type d | tee |  xargs rm -rf

mkdir test;
cd test;

mkdir -p Pumkpin/kobocha Pumkpin/WinterMelon;
touch Pumkpin/kobocha/{0..3}.txt;
touch Pumkpin/WinterMelon/{4..5}.txt;

mkdir -p Diakon/Pickle Diakon/Oden;
touch Diakon/Pickle/{6..8}.txt;
touch Diakon/Oden/{9..11}.txt;

mkdir -p Carrot/Orange Carrot/Oden;
touch Carrot/Orange/{12,13}.txt;
touch Carrot/Oden/{14,15}.txt

mkdir -p Parsenip/Chips Parsenip/Soup;

cp ../../../cpsc1280homework/Datasets/pg58.txt ./Diakon/Pickle;

cp ../../../cpsc1280homework/Datasets/pg972.txt ./Carrot;
cp ../../../cpsc1280homework/Datasets/pg972.txt ./Carrot/Oden;

cp ../../../cpsc1280homework/Datasets/heart.csv ./Pumkpin;

cp ../../../cpsc1280homework/Datasets/heart.csv Parsenip/Chips;


echo "finish";