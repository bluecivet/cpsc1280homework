#!/usr/bin/python

import os 
import sys
import shutil
import subprocess

targetDir = sys.argv[1]
targetFile = sys.argv[2]
outputFile = sys.argv[3]
f = open(outputFile, "wt")



if not os.path.exists(targetDir):
    exit(0)

if not os.path.isdir(targetDir):
    exit(0)

command = "find " + targetDir + "/ -name " + targetFile
result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
result = result.stdout
result = result.decode().split()

for file in result:
    f.write(str(file) + "\n")
    os.remove(file)



f.close()
    