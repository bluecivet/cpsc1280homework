#!/usr/bin/python

import sys
import subprocess
import os 

directory = sys.argv[1]

def runShell(command):
    result = subprocess.run(command, shell = True, stdout = subprocess.PIPE)
    return result.stdout.decode()

command = "find " + directory + " -type f | xargs ls -li"
files = runShell(command)
files = files.split("\n")
files = files[0:len(files) - 1]

for i in range(len(files)):
    files[i] = files[i].split()

files.sort(key = lambda ele:int(ele[2]), reverse = True)

top13 = files[0:13]

inodes = []
for top in top13:
    print(top[9])
    if top[0] not in inodes:
        inodes.append(top[0])
    
print("\n\nthis is all the reference \n\n")
print(inodes)

for inode in inodes:
    command = "find " + directory + " -inum " + inode
    print(command)
    subprocess.run(command, shell = True)
