#!/usr/bin/python

import sys
import os 
import subprocess

file = sys.argv[1]
text = open(file).read()

data = text.split("\n")

for i in range(len(data)):
    data[i] = data[i].split("\"")

data = data[0:len(data) - 1]
data.sort(key = lambda ele: ele[3])

outputArr = []
count = 0
for i in range(len(data)):
    # print(data[i])
    gender = data[i][11]
    year = data[i][1]
    neighborhood = data[i][7]
    neighborhoodName = data[i][9]
    number = data[i][12][1:]
    outputRow = [gender, year, neighborhood, neighborhoodName, number]
    # print(outputRow)
    outputArr.append(outputRow)
    count = count + 1

last = outputArr.pop()

last[len(last) - 1] = "Number"
outputArr = [last] + outputArr

outputText = "total birth : " + str(count) + "\n"
for i in range(len(outputArr)):
    # print(outputArr)
    outputText = outputText + ",".join(outputArr[i]) + "\n"
    
print(outputText)


