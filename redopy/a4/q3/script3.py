#!/usr/bin/python

import sys
import os 
import subprocess

d1 = sys.argv[1]
d2 = sys.argv[2]
f1 = sys.argv[3]
f2 = sys.argv[4]
f3 = sys.argv[5]


d1Temp = "d1Temp.temp"
d2Temp = "d2Temp.temp"

command = "find " + d1 + " -type f | cut -d '/' -f 2- | sort > " + d1Temp
subprocess.run(command, shell = True)

command = "find " + d2 + " -type f | cut -d '/' -f 2- | sort > " + d2Temp
subprocess.run(command, shell = True)

command = "comm -2 -3 " + d1Temp + " " + d2Temp + " > " + f1
subprocess.run(command, shell = True)

command = "comm -1 -3 " + d1Temp + " " + d2Temp + " > " + f2
subprocess.run(command, shell = True)

command = "comm -1 -2 " + d1Temp + " " + d2Temp + " > " + f3
subprocess.run(command, shell = True)

os.remove(d1Temp)
os.remove(d2Temp)