#!/usr/bin/python

import subprocess
import sys

# PRODUCT_TYPE_NAME
# PRODUCT_CLASS_NAME
# PRODUCT_SUB_CLASS_NAME
# PRODUCT_MINOR_CLASS_NAME
# PRODUCT_COUNTRY_ORIGIN_NAME
# PRODUCT_SKU_NO
# PRODUCT_LONG_NAME
# PRODUCT_BASE_UPC_NO
# PRODUCT_LITRES_PER_CONTAINER
# PRD_CONTAINER_PER_SELL_UNIT
# PRODUCT_ALCOHOL_PERCENT
# CURRENT_DISPLAY_PRICE
# SWEETNESS_CODE

def coverCsv(text):
    rows = text.split("\n")
    for i in range(len(rows)):
        rows[i] = rows[i].split(",")
    rows = rows[1:len(rows) - 1]
    return rows

countryName = sys.argv[1]
className = sys.argv[2]

f = "BC_Liquor_Store_Product_Price_List.csv"
text = open(f).read()

# text = subprocess.run("head -n 10 " + f, shell = True, stdout = subprocess.PIPE)
# text = text.stdout.decode()

data = coverCsv(text)

output = ""
for row in data:
    if row[4] == countryName and row[1] == className:
        output = output + ",".join(row) + "\n"

print(output)
        
