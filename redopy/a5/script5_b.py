#!/usr/bin/python

import re 
import sys

name = sys.argv[1]

f = open("lyrics.csv")
text = f.read()

patterm = r"[0-9]+,"+name+",[0-9]+,.+,.+,"
# print(patterm)
result = re.findall(patterm, text)

# print(result)

output = ""
for row in result:
    rowData = row.split(",")
    index = rowData[0]
    endIndex = int(index) + 1
    
    nextPatterm = r""+str(endIndex)+"+,.+,[0-9]+,.+,.+,"
    match = re.search(nextPatterm, text)
    nextText = match.group(0)
    
    start = text.find(row)
    end = text.find(nextText)
    output = output + text[start:end] + "\n\n\n"
    

print(output)

f.close()