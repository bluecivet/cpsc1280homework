#!/usr/bin/python

import sys


def getLines (num):
    line = ""
    for i in range(num):
      line = line + "-"  
    
    return line


redactPath = sys.argv[1]
redactTextPath = sys.argv[2]

redactFile = open(redactPath)
redactTextFile = open(redactTextPath)

redactWords = redactFile.read().split()
redactText = redactTextFile.read()

lineText = redactText.split("\n")

outputText = ""

for row in lineText:
    words = row.split(" ")
    count = 0
    outputLine = ""
    for word in words:
        found = False
        for redactWord in redactWords:
            if word.lower().find(redactWord.lower()) != -1:
                count = count + 1
                outputLine = outputLine + "----"
                found = True
                break
    
       
        if found == False:
            outputLine = outputLine + word
            
        outputLine= outputLine + " "
        
        if count > 1:
            # print(outputLine)
            outputLine = getLines(len(row))
            break
    outputText = outputText + outputLine + "\n"


print(outputText)