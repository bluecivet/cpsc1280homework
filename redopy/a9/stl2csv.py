#!/usr/bin/python

import sys

file = sys.argv[1]

f = open(file, "rt")

text = f.read().split()

f.close()


state = "ready"

output = []

i = 0
while i < len(text):
     
    if state == "ready":
        if text[i] ==  "solid":
            state = "start"
    
    if state == "start":
        if text[i] == "facet":
            if text[i + 1] == "normal":
                state = "readNormal"
                i = i + 1
                output.append([])
            else:
                state = "error"
                break
        elif text[i] == "endsolid":
            state = "end"
            break
            
    if state == "readNormal": 
        try:
            for j in range(3):
                output[len(output) - 1].append(str(float(text[i + j + 1])))
            state = "finishReadNormal"
            i = i + 3
        except:
            state = "error"
            break
    
    if state == "finishReadNormal":
        if text[i] == "outer":
            if text[i + 1] == "loop":
                state = "startReadPoint"
                i = i + 1
            else:
                state = "error"
                break
    
    if state == "startReadPoint" or state == "readPoint":
        if text[i] == "vertex":
            try:
                for j in range(3):
                    output[len(output) - 1].append(str(float(text[i + j + 1])))
                i = i + 3
            except:
                state = "error"
                break
        state = "readPoint"
        
    
    if state == "readPoint":
        if text[i] == "endloop":
            if text[i + 1] == "endfacet":
                state = "start"
                i = i + 1
            else:
                state = "error"
                break
    i = i + 1
    

if state == "error":
    print("some error happen")
    
if state == "end":
    outputText = ""
    for row in output:
        outputText = outputText + ",".join(row) + "\n"
    
    print(outputText)

else:
    print("state not reach to end some error happen")
