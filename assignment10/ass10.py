#!/usr/bin/python
import sys
import os


try:
    while True:
        directory = input()
        if not os.path.exists(directory):
            sys.stderr.write("the directory is not exist\n")
            continue
        if not os.path.isdir(directory):
            sys.stderr.write("this is not a directory\n")
            continue
        
        files = os.listdir(directory)
        
        sumSize = 0
        count = 0
        for file in files:
            filePath = directory + "/" + file
            if os.path.islink(filePath):
                continue
            elif os.path.isfile(filePath):
                sumSize = sumSize + os.path.getsize(filePath)
                count = count + 1
        
        print("name = " + directory + " number of file: " + str(count) + " size: " + str(sumSize))
        
except EOFError:
    print("end")