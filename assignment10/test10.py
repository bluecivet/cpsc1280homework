#!/usr/bin/python

import os 
import shutil
import subprocess

def checkFile(path):
    if os.path.exists(path):
        shutil.rmtree(path)
        
def writeFile(path):
    try:
        f1 = open(path, "wt")
        f1.write("this is some test")
        f1.write("this is some text 2")
        f1.close()
    except:
        print("error")

checkFile("test1")
checkFile("test2")
checkFile("test3")

os.mkdir("test1")
os.mkdir("test2")
os.mkdir("test3")


os.makedirs("test1/1/12")
os.mkdir("test1/2")
os.mkdir("test1/3")

writeFile("test1/1/12/f1")
writeFile("test1/2/f2")
writeFile("test1/3/f3")
writeFile("test1/f4")
writeFile("test1/f5")
writeFile("test1/f6")

subprocess.run("ln -s test1/2/f2 test1/f9.link", shell = True)


writeFile("test2/f1")
writeFile("test2/f2")
writeFile("test2/f3")
writeFile("test2/f4")
writeFile("test2/f5")
writeFile("test2/f5")




os.mkdir("test3/31")
writeFile("test3/31/file")
writeFile("test3/f1")
writeFile("test3/f2")
writeFile("test3/f3")
subprocess.run("ln -s test3/f1 test3/f1.link", shell = True)
subprocess.run("ln -s test3/f3 test3/f2.link", shell = True)
subprocess.run("ln -s test3/f1 test3/f3.link", shell = True)



