#!/bin/bash

count=0

output=""

redactWordList=($(cat $1 | xargs))


while read line
do
    count=0
    for word in $line
        do
            for redactWord in ${redactWordList[@]}
            do
                echo $word | grep -iq "$redactWord"
                if [ $? -eq 0 ]
                then
                     count=$(($count + 1))
                fi
            done    # find redact word
        done # find each word in line

    if [ $count -gt 1 ]
    then
        echo $line | sed -e 's/./-/g'
        continue    # go to next line
    else
        outputLine=$line
        for redactWord in ${redactWordList[@]}
        do
            echo $line | grep -q "$redactWord"
            if [ $? -eq 0 ] 
            then
                outputLine=$(echo $line | sed -e "s/$redactWord/----/")
            fi
        done
        echo $outputLine
    fi
    
    
done < $2