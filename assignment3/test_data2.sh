 #!/bin/bash

find ./ -name "test" -type d | tee |  xargs rm -rf

mkdir test;
cd test;

mkdir -p Pumkpin/kobocha Pumkpin/WinterMelon;
touch Pumkpin/kobocha/{0..3}.txt;
touch Pumkpin/WinterMelon/{4..5}.txt;

mkdir -p Diakon/Pickle Diakon/Oden;
touch Diakon/Pickle/{6..8}.txt;
touch Diakon/Oden/{9..11}.txt;

mkdir -p Carrot/Orange Carrot/Oden;
touch Carrot/Orange/{12,13}.txt;
touch Carrot/Oden/{14,15}.txt

mkdir -p Parsenip/Chips Parsenip/Soup;

cp ../../Datasets/pg58.txt ./Diakon/Pickle;

cp ../../Datasets/pg972.txt ./Carrot;
cp ../../Datasets/pg972.txt ./Carrot/Oden;

cp ../../Datasets/heart.csv ./Pumkpin;

cp ../../Datasets/heart.csv Parsenip/Chips;


echo "finish";