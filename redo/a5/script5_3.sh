#!/bin/bash

# "Id","District Name","Neighborhood Name","Street","Weekday","Month","Day","Hour","Part of the day","Mild injuries","Serious injuries","Victims","Vehicles involved","Longitude","Latitude"

dataPath=../../cpsc1280homework/Datasets/BarcelonaDataSet/accidents_2017.csv

accent=$(cat $dataPath | grep $1)

date=$(echo $accent | cut -d "," -f 6,7 )

cat $dataPath | grep --color $date