#!/bin/bash

# PRODUCT_TYPE_NAME
# PRODUCT_CLASS_NAME
# PRODUCT_SUB_CLASS_NAME
# PRODUCT_MINOR_CLASS_NAME
# PRODUCT_COUNTRY_ORIGIN_NAME
# PRODUCT_SKU_NO
# PRODUCT_LONG_NAME
# PRODUCT_BASE_UPC_NO
# PRODUCT_LITRES_PER_CONTAINER
# PRD_CONTAINER_PER_SELL_UNIT
# PRODUCT_ALCOHOL_PERCENT
# CURRENT_DISPLAY_PRICE
# SWEETNESS_CODE

datapath="../../cpsc1280homework/Datasets/BC_Liquor_Store_Product_Price_List.csv"

exp=",[^1](,[a-zA-Z0-9\.]+){4}$"

cat $(echo $datapath) | grep --color -E $exp