#!/bin/bash

dataPath=../../cpsc1280homework/Datasets/lyrics.csv

exp="[0-9]+,$1,[0-9]{4},.+,.+,"
# show=0
# index=-1
# nextIndex=-1

# while read line
# do
#     echo $line | grep -q -E $exp
#     if [ $? -eq 0 ]
#     then
#         show=1
#         echo $line
#         index=$(echo $line | cut -d "," -f 1)
#         nextIndex=$(($index + 1))
#         continue
#     fi
    
#     echo $line | grep -q -E "$nextIndex,.+,[0-9]{4},.+,.+,"
    
#     if [ $? -eq 0 ]
#     then
#         show=0
#     fi
    
#     if [ $show -eq 1 ]
#     then 
#         echo $line
#     fi
    
    
    
# done < $dataPath


# indexs=$(cat $dataPath | grep -E $exp | cut -d "," -f 1)

indexes=($(cat $dataPath | grep -E $exp | cut -d "," -f1))
lineNums=($(cat $dataPath | grep -nE $exp | cut -d ":" -f1))

nextIndexes=()
nextLineNUm=()

count=0
for index in ${indexes[@]}
do 
    nextPos=$((${indexes[$count]} + 1))
    nextIndexes[$count]=$nextPos
    echo $nextPos
    cat $dataPath | grep --color -nE "^$nextPos,.+,[0-9]{4},.+,.+,"
    nextLineNUm[$count]=$(cat $dataPath | grep -nE "^$nextPos,.+,[0-9]{4},.+,.+," | cut -d ":" -f1)
    count=$(($count + 1))
done

count=0
for index in ${indexes[@]}
do 
    # echo ${lineNums[$count]}
    # echo ${nextLineNUm[$count]}
    cat $dataPath | sed -n "${lineNums[$count]},${nextLineNUm[$count]}p"
    count=$(($count + 1))
    echo ""
    echo ""
    echo ""
done
