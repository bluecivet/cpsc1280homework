#!/bin/bash

files=$(find $1 -type f)

echo ^$2$ > temp
echo ^$3$ >> temp

for file in $files
do
    cat $file | grep -q $2
    if [ $? -eq 0 ]
    then
        cat $file | grep -q $3
        if [ $? -eq 0 ]
        then 
            echo $file 
        fi
        
    fi
done
