#!/bin/bash

while read line 
do
    
    echo $line | grep -q "#include<.*>"
    
    if [ $? -eq 0 ]
    then
        fileName=$(echo $line | cut -d "<" -f 2 | cut -d ">" -f 1)
        file=$(find -type f -name "$fileName")
        cat $file
    else
        echo $line
    fi
done < $1 