#!/bin/bash

dataPath=immigrants_emigrants_by_destination2.csv
countPeople=$(cat $dataPath | grep -n "^\"Ciutat Vella")

endLine=$(echo $countPeople | xargs -n1 | tail -n1 | cut -d ":" -f 1)

numPeople=$(echo $countPeople | xargs -n1 | wc -l)

echo $endLine

cat $dataPath | sed "$endLine a $numPeople"