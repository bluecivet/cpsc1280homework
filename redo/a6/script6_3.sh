#!/bin/bash

line=$(cat $1 | grep -nE "$2" | cut -d ":" -f 1)

descriptLine=$(($line + 3))

cat $1 | sed -n "$descriptLine,/^\s*$/p"