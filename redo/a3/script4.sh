#!/bin/bash
echo $1
fileList=$(find ./ -type f -name "$1")


echo "<!DOCTYPE html>"
echo "<html lang="en">"
echo "<head>"
echo "	<meta charset="UTF-8">"
echo "	<title>Document</title>"
echo "</head>"
echo "<body>"


for file in ${fileList[@]}
do
    echo "<h2> Filename: " $file "</h2>"
    echo "<pre>"
    head -n10 $file
    echo "<pre>"
    echo "<br>"
    echo "word count: " $(wc -w $file)
done

echo "</body>"
echo "</html>"