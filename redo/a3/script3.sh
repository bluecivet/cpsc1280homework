#!/bin/bash

find . -type d -name "$2" -exec rm -rf {} \;

cd $1

dirs=$(find ./ -type d)

cd ..

mkdir $2
cd $2

echo $dirs | xargs -n1 | cut -d "/" -f 2- | xargs mkdir -p

cd ../$1

files=$(find ./ -type f)

cd ..

fileArr=($(echo $files | xargs -n1 | cut -d "/" -f 2-))

linkToArr=()

iNodeNum=()

count=0

for file in ${fileArr[@]}
do 
    ln "test/"$file $2"/"$file".bak"
    linkToArr[$count]=$2"/"$file".bak"
    iNodeNum[$count]=$(ls -i "test/"$file | cut -d " " -f 1)
    count=$(($count + 1))
done

echo $(date)" scource: $1  destination: $2" >> $3

count=0
for file in ${fileArr[@]}
do
    echo ${iNodeNum[$count]} $1"/"$file $2"/"$linkToArr >> $3
    count=$(($count + 1))
done