#!/bin/bash


peopleList=($(tail -n +2 $1 | cut -d "," -f 1 | sort -u | xargs))

amount=()

echo ${peopleList[@]}

i=0
for p in ${peopleList[@]}
do 
    amount[i]=0
    i=$(($i+1))
done 



lineNum=0

while read line 
do 
    if [ $lineNum -eq 0 ]
    then
        lineNum=$(($lineNum + 1))
        continue
    fi

    p=$(echo $line | cut -d "," -f 1)
    money=$(echo $line | cut -d "," -f 6)
    saleId=$(echo $line | cut -d "," -f 2 | bc)
    
    cat $2 | grep -E "$saleId,$money"
    if [ $? -eq 0 ]
    then 
        continue
    fi 
    
    # find people index 
    index=0
    for people in ${peopleList[@]}
    do 
        if [ $people == $p ]
        then 
            break
        fi
        index=$(($index+1))
    done
    
    amount[$index]=$((${amount[$index]} + $money))
    lineNum=$(($lineNum + 1))
    
done < $1

i=0
sum=0

echo "SalesPerson ID,Total"
for p in ${peopleList[@]}
do
    echo "$p,${amount[i]}"
    sum=$(($sum + ${amount[i]}))
    i=$(($i + 1))
done
echo "Total,$sum"