#!/bin/bash

wordListArr=($(cat $1 | xargs))

count=0

outputLine=""

isFind=0;

while read line 
do 
    outputLine=""
    count=0
    for word in $line
    do
        
        for checkWord in ${wordListArr[@]}
        do
            echo $word | grep -q $checkWord
            if [ $? -eq 0 ]
            then
                count=$(($count + 1))
                isFind=1;
                break
            fi
        done
        
        if [ $isFind -eq 0 ]
        then
            outputLine=$outputLine" "$word
        else
             outputLine=$outputLine" ----"
        fi
        
        isFind=0
    done
    
    if [ $count -ge 2 ]
    then

        outputLine=$(echo $outputLine | sed 's/./-/g')
    fi
    
    echo $outputLine
    
done < $2