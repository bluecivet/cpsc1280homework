#!/bin/bash

head -n1 $1 >> temp
tail -n+2 $1 | sort -t "\"" -k4n >> temp


# gender=($(cat temp | cut -d "," -f 6))
# year=($(cat temp | cut -d "," -f 1))
# nCode=($(cat temp | cut -d "," -f 4))
# nName=($(cat temp | cut -d "," -f 5))
# nums=($(cat temp | cut -d "," -f 7))


# count=0

# for g in ${gender[@]}
# do
#     echo ${gender[$count]}","${year[$count]}","${nCode[$count]}","${nName[$count]}","${nums[$count]}
#     count=$(($count + 1))
# done


cat temp | cut -d "\"" -f 12 >> gender.tmp
cat temp | cut -d "\"" -f 2 >> year.tmp
cat temp | cut -d "\"" -f 8 >> nCode.tmp
cat temp | cut -d "\"" -f 10 >> nName.tmp
cat temp | cut -d "\"" -f 14 >> nums.tmp

paste  gender.tmp year.tmp nCode.tmp nName.tmp nums.tmp
rm -rf temp *.tmp

