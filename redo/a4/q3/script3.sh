#!/bin/bash

cd $1
find ./ -type f | sort >  ../f1.tmp
cd ..

cd $2 
find ./ -type f | sort > ../f2.tmp
cd ..

echo "this is f1.tmp"
cat f1.tmp

echo "this is f2.tmp"
cat f2.tmp

comm f1.tmp f2.tmp

comm -2 -3 f1.tmp f2.tmp > $3
comm -1 -3 f1.tmp f2.tmp > $4
comm -1 -2 f1.tmp f2.tmp > $5

rm *.tmp