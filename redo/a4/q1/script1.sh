#!/bin/bash

allFile=$(find $1 -type f | xargs -n1 ls -il | tail -n+2 | sort -t " " -k 3nr -u)


top13=$(echo $allFile | xargs -n10 | head -n 13)

echo "here is top 13 file"
echo $top13 | xargs -n10

# find all reference

inodeNumbers=$(echo $top13 | xargs -n10 | cut -d " " -f1)

echo " " 
echo " " 
echo " "
echo "here are reference"

for inumber in ${inodeNumbers[@]}
do
    find $1 -type f -inum $inumber
done