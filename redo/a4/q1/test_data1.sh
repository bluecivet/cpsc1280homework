#!/bin/bash

find -name "testDir1" -type d -prune -exec rm -rf {} \;

mkdir testDir1

cd testDir1

mkdir linkFiles files

cd files

touch file{1..20}

cd ..

cd linkFiles

mkdir link{1..20}


for i in {1..20}
do
    for ((j = 0; j < $i; j++));
    do
        ln ../files/file$i link$i/file$j.link
    done
done

cd ..
ls -l files > output.txt