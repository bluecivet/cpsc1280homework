#!/bin/bash

category=($(cat $1 | cut -d "," -f 2 | sort -u))

output="output.csv"

# output the first line
firstColumnName=$(head -n1 $1 | cut -d "," -f 1)
echo $firstColumnName $(echo ${category[@]} | tr " " ",") > $output



count=-1
while read line 
do
    count=$(($count + 1))
    if [ $count -eq 0 ]
    then
        continue
    fi
    
    leftColumn=$(echo $line | cut -d "," -f 1)
    rightColumn=$(echo $line | cut -d "," -f 2)
    rightOutput=""
    
    for cate in ${category[@]}
    do
        if [ $cate == $rightColumn ]
        then
            rightOutput=$rightOutput,1
        else
            rightOutput=$rightOutput,0
        fi
    done
    echo $leftColumn $rightOutput >> $output
    
done < $1